

import java.net.URL;

public class Contact {
    
    private String first_name, last_name, address1, address2, city,
    state, zipcode, phone_number;
    private URL facebook;
    
    public Contact(String first_name, String last_name, String address1,
                   String address2, String city, String state, String zipcode,
                   String phone_number) {
        this.first_name = first_name;
        this.last_name = last_name;
        this.address1 = address1;
        this.address2 = address2;
        this.city = city;
        this.state = state;
        this.zipcode = zipcode;
        this.phone_number = phone_number;
        //this.facebook = facebook;
    }
    
    public String getFirstName() {
        return this.first_name;
    }
    
    public String getLastName() {
        return this.last_name;
    }
    
    public String getAddress1() {
        return this.address1;
    }
    
    public String getAddress2() {
        return this.address2;
    }
    
    public String getCity() {
        return this.city;
    }
    
    public String getState() {
        return this.state;
    }
    
    public String getZipcode() {
        return this.zipcode;
    }
    
    public String getPhoneNumber() {
        return this.phone_number;
    }
    
    /*public URL getFacebook() {
     return this.facebook;
     }*/
    
    public void setFirstName(String s) {
        this.first_name = s;
    }
    
    public void setLastName(String s) {
        this.last_name = s;
    }
    
    public void setAddress1(String s) {
        this.address1 = s;
    }
    
    public void setAddress2(String s) {
        this.address2 = s;
    }
    
    public void setCity(String s) {
        this.city = s;
    }
    
    public void setState(String s) {
        this.state = s;
    }
    
    public void setZipcode(String s) {
        this.zipcode = s;
    }
    
    public void setPhoneNumber(String s) {
        this.phone_number = s;
    }
    
    /*public void setFacebook(URL url) {
     this.facebook = url;
     }*/
    
    public String toString() {
        /* Returns string containing contact as USPS approved postal address
         * Last name and zipcode are required, rest is optional
         */
        StringBuilder stringBuilder = new StringBuilder();
        if (!this.first_name.isEmpty() && this.first_name != null) {
            stringBuilder.append(this.first_name);
            stringBuilder.append(" ");
        }
        //Last name required
        stringBuilder.append(this.last_name);
        stringBuilder.append("\n");
        if (!this.address1.isEmpty() && this.address1 != null) {
            stringBuilder.append(this.address1);
            stringBuilder.append("\n");
        }
        if (!this.address2.isEmpty() && this.address2 != null) {
            stringBuilder.append(this.address2);
            stringBuilder.append("\n");
        }
        if (!this.city.isEmpty() && this.city != null) {
            stringBuilder.append(this.city);
            stringBuilder.append(", ");
        }
        if (!this.state.isEmpty() && this.state != null) {
            stringBuilder.append(this.state);
            stringBuilder.append(" ");
        }
        //Zipcode required
        stringBuilder.append(this.zipcode);
        return stringBuilder.toString();
    }
    
    public static boolean isZipcode(String s) {
        /* Checks if zipcode is a valid zipcode
         * Only allows US standard zipcode to be valid
         */
        return s.matches("^[0-9]{5}(?:-[0-9]{4})?$");
    }
    
}
