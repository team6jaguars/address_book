

import java.awt.*;
import javax.swing.*;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.awt.event.ActionListener;

public class MyFrame extends JFrame{
	JPanel mypanel;
	JPanel mypanel2;
	JPanel mypanel3;
	JPanel radioButton;
	
	public Address_Book book;
	ArrayList<Contact> contacts = new ArrayList<Contact>();
	private static int index = 0;
	private final JSplitPane splitPane;
	private final JSplitPane sp2;
	private static  Contact _contact;
	private JFrame frame2;
	JLabel label  = new JLabel("Contacts");
	private final JLabel sort = new JLabel("Sort by:");
	public static JButton add_button = new JButton("add");
	public static JButton delete_button = new JButton("delete");
	
	//checkbox
	
	final JRadioButton lastBox = new JRadioButton("Last Name");
	final JRadioButton zipBox = new JRadioButton("Zip");
	final JRadioButton addressBox = new JRadioButton("Address");
	ButtonGroup group = new ButtonGroup();
	
	
	
	//text field to add contact
	public static JTextField _name = new JTextField("");
	public static JTextField _lastname = new JTextField("");
	public static JTextField _address = new JTextField("");
	public static JTextField _address2 = new JTextField("");
	public static JTextField _zip = new JTextField("");
	public static JTextField _city = new JTextField("");
	public static JTextField _state = new JTextField("");
	public static JTextField _phone_number = new JTextField("");
	
	public static JLabel label_name = new JLabel("First Name");
	public static JLabel label_lastname = new JLabel("Last");

	public static JLabel label_address = new JLabel("Address");
	public static JLabel label_zip = new JLabel("Zip");
	public static JLabel label_city = new JLabel("City");
	public static JLabel label_state = new JLabel("State");
	public static JLabel label_phone_number = new JLabel("Phone #");
	
	
	public static JButton _edit = new JButton("edit");
	public static JButton _save = new JButton("save");
	public static JButton _new = new JButton("New Contact");
	private static Boolean isNew = false;
	public static JLabel label_address2 = new JLabel("Address");
	
	//display of contacts
	
	public static JButton contact1 = new JButton("Empty");
	public static JButton contact2 = new JButton("Empty");
	public static JButton contact3 = new JButton("Empty");
	public static JButton contact4 = new JButton("Empty");
	public static JButton contact5 = new JButton("Empty");
	public static JButton contact6 = new JButton("Empty");

	public static JButton up = new JButton("^");
	public static JButton down = new JButton("v");


	private static String first_name;
	private static String last_name;
	private static String address1;
	private static String address2;
	private static String city;
	private static String state;
	private static String zipcode;
	private static String phone_number;
	private static boolean disable_buttons = true;
	public MyFrame(String title){
		super(title);
		book = new Address_Book(contacts);
		setSize(600, 400);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		frame2 = new JFrame("Contact ");
		frame2.setBounds(100, 100, 450, 300);
		//frame2.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		radioButton = new JPanel();

		radioButton.add(sort);
		radioButton.add(lastBox);
		radioButton.add(zipBox);
		group.add(lastBox);
		group.add(zipBox);
		
		lastBox.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				book.sort_last();
				setContactList();
				
			}
		});
		zipBox.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				book.sort_zip();
				setContactList();
				
			}
		});
		
		mypanel =  new JPanel(new GridLayout(10,1, 10 , 10));
		mypanel2 = new JPanel(new GridBagLayout());
		

		GridBagConstraints c = new GridBagConstraints();
		c.fill = GridBagConstraints.HORIZONTAL;
		
		c.gridx = 0;
		c.gridy = 0;
		mypanel2.add(label_name, c);
		
		c.weightx = 1;
		c.gridx = 1;
		c.gridy = 0;
		mypanel2.add(_name, c);


		c.weightx = 0;
		c.gridx = 2;
		c.gridy = 0;
		mypanel2.add(label_lastname, c);

		c.weightx = 1;
		c.gridx = 3;
		c.gridy = 0;
		mypanel2.add(_lastname, c);
		

		c.weightx = 0;
		c.gridx = 0;
		c.gridy = 1;
		mypanel2.add(label_address, c);
		

		c.weightx = 1;
		c.gridx = 1;
		c.gridy = 1;
		c.gridwidth = 3;
		mypanel2.add(_address, c);
		

		c.weightx = 0;
		c.gridx = 0;
		c.gridy = 2;
		mypanel2.add(label_address2, c);
		

		c.weightx = 1;
		c.gridx = 1;
		c.gridy = 2;
		c.gridwidth = 3;
		mypanel2.add(_address2, c);


		c.weightx = 0;
		c.gridx = 0;
		c.gridy = 3;
		c.gridwidth = 1;
		mypanel2.add(label_city, c);
		

		c.weightx = 1;
		c.gridx = 1;
		c.gridy = 3;
		c.gridwidth = 1;
		mypanel2.add(_city, c);
		

		c.weightx = 0;
		c.gridx = 2;
		c.gridy = 3;
		c.gridwidth = 1;
		mypanel2.add(label_state, c);
		

		c.weightx = 1;
		c.gridx = 3;
		c.gridy = 3;
		c.gridwidth = 1;
		mypanel2.add(_state, c);
		

		c.weightx = 0;
		c.gridx = 0;
		c.gridy = 4;
		c.gridwidth = 1;
		mypanel2.add(label_zip, c);
		

		c.weightx = 1;
		c.gridx = 1;
		c.gridy = 4;
		c.gridwidth = 1;
		mypanel2.add(_zip, c);
		

		c.weightx = 0;
		c.gridx = 2;
		c.gridy = 4;
		c.gridwidth = 1;
		mypanel2.add(label_phone_number, c);
		

		c.weightx = 1;
		c.gridx = 3;
		c.gridy = 4;
		c.gridwidth = 1;
		mypanel2.add(_phone_number, c);
		

		c.weightx = 0;
		c.gridx = 0;
		c.gridy = 5;
		c.gridwidth = 1;
		mypanel2.add(delete_button, c);


		c.weightx = 0;
		c.gridx = 1;
		c.gridy = 5;
		c.gridwidth = 1;
		mypanel2.add(_edit, c);


		c.weightx = 0;
		c.gridx = 2;
		c.gridy = 5;
		c.gridwidth = 1;
		mypanel2.add(_save, c);
		

		
		mypanel.add(label);
		mypanel.add(_new);
		_new.setBackground(Color.cyan);
		mypanel.add(contact1);
		mypanel.add(contact2);
		mypanel.add(contact3);
		mypanel.add(contact4);
		mypanel.add(contact5);
		mypanel.add(contact6);

		mypanel.add(up);
		up.setBackground(Color.green);
		mypanel.add(down);
		down.setBackground(Color.green);
		
		sp2 = new JSplitPane(JSplitPane.VERTICAL_SPLIT, false, mypanel2, radioButton);
		sp2.setDividerLocation(200);
		splitPane = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT, false, mypanel,sp2);
		splitPane.setDividerLocation(175);
		getContentPane().add(splitPane);
		disableEditing();
		

		up.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent e) {
			if(getIndex() > 0) {
				index--;
				setContactList();
			}
			}
		});


		down.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent e) {
			if(getIndex() < (book.size() - 6)) {
				index++;
				setContactList();
			}
			}
		});
		delete_button.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e){
				book.delete_contact(_contact);
				clearText();
				disableEditing();
				setContactList();
			}
		});
		
		//makes contacts editable
		_edit.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e){
				enableEditing();
			}
		});
		_new.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				disable_buttons = true;
				isNew = true;
				clearText();
				enableEditing();
			}
		});
		//saves contact after editing
		_save.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e){
			if(isNew){
				disable_buttons = true;
				first_name = _name.getText();
				last_name = _lastname.getText();
				address1 = _address.getText();
				address2 = _address2.getText();
				city = _city.getText();
				state = _state.getText();
				zipcode = _zip.getText();
				phone_number = _phone_number.getText();
				
				if(!isContact()) {
					System.out.println("not real in _save if");
					return;
				}
				Contact person = new Contact(first_name, last_name, address1,
				address2, city, state, zipcode, phone_number);
				book.add_contact(person);
				_name.setText("");
				_lastname.setText("");
				_address.setText("");
				_address2.setText("");
				_city.setText("");
				_state.setText("");
				_zip.setText("");
				_phone_number.setText("");
				isNew = false;
				disableEditing();
				setContactList();
			}else {
			_contact.setFirstName(_name.getText());
			_contact.setLastName(_lastname.getText());
			_contact.setAddress1(_address.getText());
			_contact.setAddress2(_address2.getText());
			_contact.setZipcode(_zip.getText());
			_contact.setCity(_city.getText());
			_contact.setState(_state.getText());
			_contact.setPhoneNumber(_phone_number.getText());

			disableEditing();
			setContactList();
			disableEditing();
			}
			
			}
		});
		
		
		contact1.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e){
				if(disable_buttons) {
				Contact first_contact = book.get_contact(getIndex());
				set_current_contact(book.get_contact(getIndex()));
				_name.setText(first_contact.getFirstName());
				_lastname.setText(first_contact.getLastName());
				_address.setText(first_contact.getAddress1());
				_address2.setText(first_contact.getAddress2());
				_zip.setText(first_contact.getZipcode());
				_city.setText(first_contact.getCity());
				_state.setText(first_contact.getState());
				_phone_number.setText(first_contact.getPhoneNumber());
				set_current_contact(first_contact);
//				frame2.setVisible(true);
				isNew = false;

				disableEditing();
				}

			}
		});		
		
		contact2.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e){
				if(disable_buttons) {
				Contact first_contact = book.get_contact(getIndex()+1);
				set_current_contact(book.get_contact(getIndex()+1));
				_name.setText(first_contact.getFirstName());
				_lastname.setText(first_contact.getLastName());
				_address.setText(first_contact.getAddress1());
				_address2.setText(first_contact.getAddress2());
				_zip.setText(first_contact.getZipcode());
				_city.setText(first_contact.getCity());
				_state.setText(first_contact.getState());
				_phone_number.setText(first_contact.getPhoneNumber());
				set_current_contact(first_contact);
//				frame2.setVisible(true);
				isNew = false;
				disableEditing();
				}
			}
		});
		
		contact3.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e){
				if(disable_buttons) {
				Contact first_contact = book.get_contact(getIndex()+2);
				set_current_contact(book.get_contact(getIndex()+2));
				_name.setText(first_contact.getFirstName());
				_lastname.setText(first_contact.getLastName());
				_address.setText(first_contact.getAddress1());
				_zip.setText(first_contact.getZipcode());
				_address2.setText(first_contact.getAddress2());
				_city.setText(first_contact.getCity());
				_state.setText(first_contact.getState());
				_phone_number.setText(first_contact.getPhoneNumber());
				set_current_contact(first_contact);
//				frame2.setVisible(true);
				isNew = false;

				disableEditing();
				}
			}
		});
		
		contact4.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e){
				if(disable_buttons) {
				Contact first_contact = book.get_contact(getIndex()+3);
				set_current_contact(book.get_contact(getIndex()+3));
				_name.setText(first_contact.getFirstName());
				_lastname.setText(first_contact.getLastName());
				_address.setText(first_contact.getAddress1());
				_address2.setText(first_contact.getAddress2());
				_zip.setText(first_contact.getZipcode());
				_city.setText(first_contact.getCity());
				_state.setText(first_contact.getState());
				_phone_number.setText(first_contact.getPhoneNumber());
				set_current_contact(first_contact);
//				frame2.setVisible(true);
				isNew = false;

				disableEditing();
				}
			}
		});
		
		contact5.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e){
				if(disable_buttons) {
				Contact first_contact = book.get_contact(getIndex()+4);
				set_current_contact(book.get_contact(getIndex()+4));
				_name.setText(first_contact.getFirstName());
				_lastname.setText(first_contact.getLastName());
				_address.setText(first_contact.getAddress1());
				_zip.setText(first_contact.getZipcode());
				_city.setText(first_contact.getCity());
				_state.setText(first_contact.getState());
				_address2.setText(first_contact.getAddress2());
				_phone_number.setText(first_contact.getPhoneNumber());
				set_current_contact(first_contact);
//				frame2.setVisible(true);
				isNew = false;
				disableEditing();
				}
			}
		});
		
		contact6.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e){
				if(disable_buttons) {
				Contact first_contact = book.get_contact(getIndex()+5);
				set_current_contact(book.get_contact(getIndex()+5));
				_name.setText(first_contact.getFirstName());
				_lastname.setText(first_contact.getLastName());
				_address.setText(first_contact.getAddress1());
				_zip.setText(first_contact.getZipcode());
				_address2.setText(first_contact.getAddress2());
				_city.setText(first_contact.getCity());
				_state.setText(first_contact.getState());
				_phone_number.setText(first_contact.getPhoneNumber());
//				frame2.setVisible(true);
				isNew = false;
				disableEditing();
				}
			}
		});
		
		
		
		
		MyFrame.add_button.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e){
				first_name = _name.getText();
				last_name = _lastname.getText();
				address1 = _address.getText();
				address2 = _address2.getText();
				city = _city.getText();
				state = _state.getText();
				zipcode = _zip.getText();
				phone_number = _phone_number.getText();
				if(first_name.isEmpty() && last_name.isEmpty()) {
					System.out.println("no name provided");
					return;
				}
				Contact person = new Contact(first_name, last_name, address1,
				address2, city, state, zipcode, phone_number);
				book.add_contact(person);
				_name.setText("");
				_lastname.setText("");
				_address.setText("");
				_address2.setText("");
				_city.setText("");
				_state.setText("");
				_zip.setText("");
				_phone_number.setText("");
				setContactList();
			}
		});
		
	}
	
	public void set_current_contact(Contact contact) {
		_contact = contact;
		
	}
	
	
	public int getIndex() {
		
		return index;
		
	}
	public void setContactList() {
		//System.out.println(book.size());
		if(book.size() == 0) {
			contact1.setText("Empty");
			return;
		}
		contact1.setText(book.get_contact(index).getFirstName() + " " +book.get_contact(index).getLastName());
		
		if(book.size() > 1) {
			contact2.setText(book.get_contact(index+ 1).getFirstName() + " " +book.get_contact(index+1).getLastName());
		}else {
			contact2.setText("Empty");
		}
		if(book.size() >2) {
			contact3.setText(book.get_contact(index + 2).getFirstName() + " " +book.get_contact(index+2).getLastName());
		}else {
			contact3.setText("Empty");
		}
		if(book.size() > 3) {
			contact4.setText(book.get_contact(index + 3).getFirstName() + " " +book.get_contact(index+3).getLastName());
		}else {
			contact4.setText("Empty");
		}
		if(book.size() > 4) {
			contact5.setText(book.get_contact(index + 4).getFirstName() + " " +book.get_contact(index+4).getLastName());
		}else {
			contact5.setText("Empty");
		}
		if(book.size() > 5) {
			contact6.setText(book.get_contact(index + 5).getFirstName() + " " +book.get_contact(index+5).getLastName());
		}else {
			contact6.setText("Empty");
		}
}

	public void enableEditing() {
		_name.setEditable(true);
		_lastname.setEditable(true);
		_address.setEditable(true);
		_address2.setEditable(true);
		_zip.setEditable(true);
		_city.setEditable(true);
		_state.setEditable(true);
		_phone_number.setEditable(true);
		
	}
	public void clearText() {
		_name.setText("");
		_lastname.setText("");
		_address.setText("");
		_zip.setText("");
		_address2.setText("");
		_city.setText("");
		_state.setText("");
		_phone_number.setText("");
	}
	
	public boolean isContact() {
		int counter = 0;
		
		if(!_address.getText().isEmpty()) {
			counter++;
		}
		if(!_address2.getText().isEmpty()) {
			counter++;
		}
		if(!_city.getText().isEmpty()) {
			counter++;
		}
		if(!_state.getText().isEmpty()) {
			counter++;
		}
		if(!_zip.getText().isEmpty()) {
			counter++;
		}
		if(!_phone_number.getText().isEmpty()) {
			counter++;
		}
		if(counter == 0) {
			System.out.println("no additional fields");
			return false;
		}
		if(first_name.isEmpty() && last_name.isEmpty()) {
			System.out.println("no name provided");
			return false;
		}
		return true;
	}
	
	public void disableEditing() {
		_name.setEditable(false);
		_lastname.setEditable(false);
		_address.setEditable(false);
		_address2.setEditable(false);
		_zip.setEditable(false);
		_city.setEditable(false);
		_state.setEditable(false);
		_phone_number.setEditable(false);
		
	}
	
}
