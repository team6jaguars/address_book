package Address_Book;
import java.io.*;
import java.util.Arrays;

public class ParseFile {
    public static Address_Book FileToAddressBook(String fileName) {
        
        // The name of the file to open.
        String[] parsed_line = null;
        
        String first_name, last_name, address1, address2, city, state,
        zipcode, phone_number;
        
        // This will reference one line at a time
        String line = null;
        
        Address_Book book = new Address_Book(Address_Book.new_book());
        
        try {
            // FileReader reads text files in the default encoding.
            FileReader fileReader = new FileReader(fileName);
            
            // Always wrap FileReader in BufferedReader.
            BufferedReader bufferedReader = new BufferedReader(fileReader);
            
            while((line = bufferedReader.readLine()) != null) {
                parsed_line = line.split("\t");
                
                city = parsed_line[0];
                state = parsed_line[1];
                zipcode = parsed_line[2];
                address1 = parsed_line[3];
                address2 = parsed_line[4];
                last_name = parsed_line[5];
                first_name = parsed_line[6];
                phone_number = parsed_line[7];
                
                book.add_contact(new Contact(first_name, last_name, address1,
                                             address2, city, state, zipcode, phone_number));
                
                /*System.out.println(line);
                 //0:First, 1:Last, 2:Address 3:City, State, 4:Zipcode
                 System.out.println("Parsed line: " + Arrays.toString(parsed_line));
                 */
            }
            
            // Always close files.
            bufferedReader.close();
        }
        catch(FileNotFoundException ex) {
            System.out.println(
                               "Unable to open file '" +
                               fileName + "'");
            ex.printStackTrace();
        }
        catch(IOException ex) {
            System.out.println(
                               "Error reading file '"
                               + fileName + "'");
            //Or we could just do this:
            ex.printStackTrace();
        }
        
        return book;
    }
    
    public static void SaveToFile(String filename, Address_Book book) {
        try (Writer writer = new BufferedWriter(new OutputStreamWriter(
                                                                       new FileOutputStream(filename), "utf-8"))) {
            Contact contact;
            String first_name, last_name, address1, address2, city, state,
            zipcode, phone_number;
            for (int i = 0; i < book.size(); i++) {
                contact = book.get_contact(i);
                first_name = contact.getFirstName();
                last_name = contact.getLastName();
                address1 = contact.getAddress1();
                address2 = contact.getAddress2();
                city = contact.getCity();
                state = contact.getState();
                zipcode = contact.getZipcode();
                phone_number = contact.getPhoneNumber();
                StringBuilder sb = new StringBuilder();
                sb.append(city);
                sb.append("\t");
                sb.append(state);
                sb.append("\t");
                sb.append(zipcode);
                sb.append("\t");
                sb.append(address1);
                sb.append("\t");
                sb.append(address2);
                sb.append("\t");
                sb.append(last_name);
                sb.append("\t");
                sb.append(first_name);
                sb.append("\t");
                sb.append(phone_number);
                sb.append("\n");
                writer.write(sb.toString());
            }
        }
        catch(IOException ex) {
            System.out.println(
                               "Error reading file '" 
                               + filename + "'");                  
            //Or we could just do this: 
            ex.printStackTrace();
        }
    }
    
    public static String CreateFile(String name) throws IOException{
        String filename = name;
        String directory = System.getProperty("user.dir");
        String filepath = directory + File.separator + filename;
        int index = 0;
        File file = new File(filepath+".tsv");
        while (!file.createNewFile()) {
            index++;
            filename = name+index;
            filepath = directory + File.separator + filename;
            file = new File(filepath+".tsv");
        }
        return filename;
    }
}
