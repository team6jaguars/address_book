

import java.util.ArrayList;
import java.util.Collections;

public class Address_Book {
    
    ArrayList<Contact> addressbook;
    
    public Address_Book(ArrayList<Contact> addressbook) {
        this.addressbook = addressbook;
    }
    
    public static ArrayList<Contact> new_book() {
        ArrayList<Contact> new_addbook = new ArrayList<Contact>();
        return new_addbook;
    }
    
    public void add_contact(Contact contact) {
        addressbook.add(contact);
    }
    
    public Contact get_contact(int index) {
        return addressbook.get(index);
    }
    
    public void delete_contact(Contact contact) {
        addressbook.remove(contact);
    }
    
    public void sort_zip() {
        Collections.sort(addressbook,
                         (c1, c2) -> c1.getZipcode().compareTo(c2.getZipcode()));
    }
    
    public void sort_last() {
        Collections.sort(addressbook,
                         (c1, c2) -> c1.getLastName().compareTo(c2.getLastName()));
    }
    
    public int size() {
        /* Used for AddressBookTestMain.java
         * If never used for application then delete
         */
        return addressbook.size();
    }
    
    public ArrayList<Contact> search_contact(String s) {
        if (addressbook.size() == 0) {
            return null;
        }
        ArrayList<Contact> found = new ArrayList<Contact>();
        for (int i = 0; i < addressbook.size(); i++) {
            if (addressbook.get(i).getFirstName().toLowerCase().matches("(.*)"+s.toLowerCase()+"(.*)") ||
                addressbook.get(i).getLastName().toLowerCase().matches("(.*)"+s.toLowerCase()+"(.*)")) {
                found.add(addressbook.get(i));
            }
        }
        return found;
    }
    
}
