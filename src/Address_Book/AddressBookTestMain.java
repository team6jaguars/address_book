package Address_Book;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;

public class AddressBookTestMain {
    
    public static void main(String[] args) throws MalformedURLException {
        
        Address_Book book = new Address_Book(Address_Book.new_book());
        
        ArrayList<Contact> found1 = new ArrayList<Contact>();
        ArrayList<Contact> found2 = new ArrayList<Contact>();
        ArrayList<Contact> found3 = new ArrayList<Contact>();
        
        String first_name = "first";
        String last_name = "last";
        String address1 = "address1";
        String address2 = "address2";
        String city = "city";
        String state = "state";
        String zipcode = "000000";
        String phone_number = "0000000000";
        URL facebook = new URL("https://www.google.com");
        
        Contact contact1 = new Contact(first_name, last_name, address1,
                                       address2, city, state, zipcode, phone_number, facebook);
        Contact contact2 = new Contact(first_name, last_name, address1,
                                       address2, city, state, zipcode, phone_number, facebook);
        Contact contact3 = new Contact(first_name, last_name, address1,
                                       address2, city, state, zipcode, phone_number, facebook);
        
        System.out.println("Test add_contact(), toString(), "
                           + "getter functions");
        
        book.add_contact(contact1);
        book.add_contact(contact2);
        book.add_contact(contact3);
        
        System.out.println(book.get_contact(0).toString());
        System.out.println(book.get_contact(0).getPhoneNumber());
        System.out.println(book.get_contact(0).getFacebook().toString());
        
        System.out.println("Test setter functions");
        
        book.get_contact(0).setFirstName("Lucas");
        book.get_contact(0).setLastName("Chavarria");
        book.get_contact(0).setAddress1("73 E 19th Ave");
        book.get_contact(0).setAddress2("Apt 5");
        book.get_contact(0).setCity("Eugene");
        book.get_contact(0).setState("OR");
        book.get_contact(0).setZipcode("97401");
        book.get_contact(0).setPhoneNumber("8189034424");
        book.get_contact(0).setFacebook(new URL("https://www.facebook.com"));
        
        System.out.println(book.get_contact(0).toString());
        System.out.println(book.get_contact(0).getPhoneNumber());
        System.out.println(book.get_contact(0).getFacebook().toString());
        System.out.println();
        
        System.out.println("Check if all contacts were added via add_contact()");
        
        for (int i = 0; i < book.size(); i++) {
            System.out.println(book.get_contact(i).toString());
            System.out.println(book.get_contact(i).getPhoneNumber());
            System.out.println(book.get_contact(i).getFacebook().toString());
        }
        System.out.println();
        
        System.out.println("Test sort_zip()");
        
        book.sort_zip();
        
        for (int i = 0; i < book.size(); i++) {
            System.out.println(book.get_contact(i).toString());
            System.out.println(book.get_contact(i).getPhoneNumber());
            System.out.println(book.get_contact(i).getFacebook().toString());
        }
        System.out.println();
        
        System.out.println("Test sort_last()");
        
        book.sort_last();
        
        for (int i = 0; i < book.size(); i++) {
            System.out.println(book.get_contact(i).toString());
            System.out.println(book.get_contact(i).getPhoneNumber());
            System.out.println(book.get_contact(i).getFacebook().toString());
        }
        System.out.println();
        
        System.out.println("Test search_contact(\"Lucas\") for name search");
        
        found1 = book.search_contact("Lucas");
        
        for (int i = 0; i < found1.size(); i++) {
            System.out.println(book.get_contact(i).toString());
            System.out.println(book.get_contact(i).getPhoneNumber());
            System.out.println(book.get_contact(i).getFacebook().toString());
        }
        System.out.println();
        
        System.out.println("Test search_contact(\"a\") for partial name search");
        
        found2 = book.search_contact("a");
        
        for (int i = 0; i < found2.size(); i++) {
            System.out.println(book.get_contact(i).toString());
            System.out.println(book.get_contact(i).getPhoneNumber());
            System.out.println(book.get_contact(i).getFacebook().toString());
        }
        System.out.println();
        
        System.out.println("Test search_contact(\"l\") for case insensitivity"); 
        
        found3 = book.search_contact("l");
        
        for (int i = 0; i < found3.size(); i++) {
            System.out.println(book.get_contact(i).toString());
            System.out.println(book.get_contact(i).getPhoneNumber());
            System.out.println(book.get_contact(i).getFacebook().toString());
        }
    }
    
}
